import React, {Component} from 'react';
import Enter from "./components/Enter/Enter";
import {inject, observer} from "mobx-react";
import ApiStore from "./store/ApiStore";
import RootStore from "./store";

import "./App.sass"
import EnterStore from "./store/EnterStore";
import Chat from "./components/Chat/Chat";

interface Props {
    root?: RootStore;
}

@inject("root")
@observer
class App extends Component<Props> {
    get enterStore(): EnterStore {
        return this.props.root!.enterStore;
    }

    get apiStore(): ApiStore {
        return this.props.root!.apiStore;
    }

    render() {
        const {serverStatus} = this.apiStore;
        const {authorized} = this.enterStore;
        return (
            <div className="app">
                <div className={`server_status ${serverStatus ? "connected" : "disconnected"}`}>
                    Сервер: {serverStatus ? "Соединение установлено" : "Соединение отсутствует"}
                </div>
                {
                    serverStatus &&
                        <div className={"content"}>
                            {authorized ? <Chat/> : <Enter/>}
                        </div>
                }
            </div>
        );
    }
}

export default App;
