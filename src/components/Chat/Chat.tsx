import React, {ChangeEvent, Component, KeyboardEvent} from "react";
import RootStore from "../../store";
import {inject, observer} from "mobx-react";
import ChatStore from "../../store/ChatStore";
import UserList from "./UserList";
import "./Chat.sass";
import {GENERAL_CHAT} from "../../store/ChatStore/types";
import Messages from "./Messages";

interface Props {
    root?: RootStore;
}

@inject("root")
@observer
class Chat extends Component<Props> {
    get chatStore(): ChatStore {
        return this.props.root!.chatStore;
    }

    handleText = (ev: ChangeEvent) => {
        this.chatStore.text = (ev.target as HTMLTextAreaElement).value;
    };

    handleEnterPress = (ev: KeyboardEvent<HTMLTextAreaElement>) => {
        if (ev.key == "Enter") {
            this.chatStore.sendMessage();
        }
    };

    render() {
        const {nickname, currentChatName, newMessage, sendMessage, text, logout, setCurrentChat, toggleSelectMode, forwardMessages} = this.chatStore;
        return (
            <div className={"chat"}>
                <div className={"nickname"}>{nickname}</div>
                <UserList/>
                <div className={`general_chat ${newMessage && "new_message"}`} onClick={() => setCurrentChat(GENERAL_CHAT)}>{GENERAL_CHAT}</div>
                <div className={"logout"} onClick={logout}>Выйти</div>

                <div className={"current_chat"}>{currentChatName}</div>
                <Messages/>

                <button type={"button"} className={"send"} onClick={sendMessage}>Отправить</button>
                <button type={"button"} className={"select"} onClick={toggleSelectMode}>Выбрать</button>
                <button type={"button"} className={"forward"} onClick={forwardMessages}>Переслать</button>

                <textarea name="text" id="text" value={text} onChange={this.handleText}
                          placeholder={"Введите сообщение..."} onKeyPress={this.handleEnterPress}/>
            </div>
        );
    }
}

export default Chat;
