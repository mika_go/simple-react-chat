import {ChatMessage, chatMessageToString} from "../../store/ChatStore/types";
import {inject, observer} from "mobx-react";
import React, {Component} from "react";
import ChatStore from "../../store/ChatStore";
import RootStore from "../../store";

const WHITE_CIRCLE = "○";
const BLACK_CIRCLE = "●";

interface MessageProps {
    root?: RootStore;
    index: number;
    value: ChatMessage;
}

@inject("root")
@observer
class Message extends Component<MessageProps> {
    get chatStore(): ChatStore {
        return this.props.root!.chatStore;
    }

    select = () => {
        const {selectMessage, unselectMessage} = this.chatStore;
        const {value} = this.props;
        if (value.selected) {
            unselectMessage(value)
        } else {
            selectMessage(value);
        }
        value.selected = !value.selected;
    };

    render() {
        const {nickname, selectMode, sortedCurrentChat} = this.chatStore;
        const {index, value} = this.props;
        const text = chatMessageToString(value, true);
        return (
            <div className={`message ${value.from === nickname && "own_message"}`} onClick={this.select} ref={ref => {
                if (ref && index === sortedCurrentChat.length - 1) ref.scrollIntoView({behavior: "smooth"});
            }}>
                <div>{text[0]}<br/>{text[1]}</div>
                {selectMode && <div className={"checker"}>{value.selected ? BLACK_CIRCLE : WHITE_CIRCLE}</div>}
            </div>
        );
    }
}

export default Message;
