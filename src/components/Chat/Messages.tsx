import React, {Component} from "react";
import RootStore from "../../store";
import {inject, observer} from "mobx-react";
import ChatStore from "../../store/ChatStore";
import Message from "./Message";

interface Props {
    root?: RootStore
}

@inject("root")
@observer
class Messages extends Component<Props> {
    get chatStore(): ChatStore {
        return this.props.root!.chatStore;
    }

    render() {
        const {sortedCurrentChat} = this.chatStore;
        return (
            <div className={"messages"}>
                {sortedCurrentChat.map((value, index) => <Message key={index} index={index} value={value}/>)}
            </div>
        );
    }
}

export default Messages;
