import React, {Component} from "react";
import RootStore from "../../store";
import {inject, observer} from "mobx-react";
import ChatStore from "../../store/ChatStore";

interface Props {
    root?: RootStore
}

@inject("root")
@observer
class UserList extends Component<Props> {
    get chatStore(): ChatStore {
        return this.props.root!.chatStore;
    }

    render() {
        const {nickname, sortedUsers, setCurrentChat} = this.chatStore;
        return (
            <div className={"user_list"}>
                {sortedUsers.map((value, index) =>
                    <div
                        className={`user ${value.nickname === nickname && "current"} ${value.newMessage && "new_message"}`}
                        key={index}
                        onClick={() => setCurrentChat(value.nickname)}>{value.nickname}</div>
                )}
            </div>
        );
    }
}

export default UserList;
