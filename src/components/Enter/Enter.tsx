import React, {ChangeEvent, Component, KeyboardEvent} from "react";
import {inject, observer} from "mobx-react";
import RootStore from "../../store";
import EnterStore from "../../store/EnterStore";
import {CurrentState} from "../../store/EnterStore/types";
import {computed} from "mobx";

import "./Enter.sass"

interface Props {
    root?: RootStore;
}

@inject("root")
@observer
class Enter extends Component<Props> {
    @computed
    get header(): string {
        const {state} = this.enterStore;
        switch (state) {
            case CurrentState.Authorization:
                return "Авторизация";
            case CurrentState.Registration:
                return "Регистрация";
        }
    }

    @computed
    get okButton(): string {
        const {state} = this.enterStore;
        switch (state) {
            case CurrentState.Authorization:
                return "Войти";
            case CurrentState.Registration:
                return "Продолжить";
        }
    }

    @computed
    get okButtonClick(): () => void {
        const {state, authorize, register} = this.enterStore;
        switch (state) {
            case CurrentState.Authorization:
                return authorize;
            case CurrentState.Registration:
                return register;
        }
    }

    @computed
    get changeButton(): string {
        const {state} = this.enterStore;
        switch (state) {
            case CurrentState.Authorization:
                return "Регистрация";
            case CurrentState.Registration:
                return "Авторизация";
        }
    }

    get enterStore(): EnterStore {
        return this.props.root!.enterStore;
    }

    handleNicknameInput = (ev: ChangeEvent) => {
        this.enterStore.nickname = (ev.target as HTMLInputElement).value;
    };

    handlePasswordInput = (ev: ChangeEvent) => {
        this.enterStore.password = (ev.target as HTMLInputElement).value;
    };

    handleEnterPress = (ev: KeyboardEvent<HTMLInputElement>) => {
        const {nickname, password} = this.enterStore;
        if (ev.key == "Enter") {
            this.enterStore.error = "";
            if (nickname === "") {
                this.enterStore.error = "Введите логин";
                return;
            }
            if (password === "") {
                this.enterStore.error = "Введите пароль";
                return;
            }
            this.okButtonClick();
        }
    };

    render() {
        const {nickname, password, changeState, error, info} = this.enterStore;
        return (
            <div className={"enter_block"}>
                <div className={"enter"}>
                    <header>{this.header}</header>
                    <main>
                        <div className={"block"}>
                            <label htmlFor={"nickname"}>Логин:</label>
                            <input type="text" name={"nickname"} id={"nickname"} value={nickname}
                                   placeholder={"Введите логин..."} onChange={this.handleNicknameInput}
                                   onKeyPress={this.handleEnterPress} autoComplete={"off"} autoFocus={true}/>
                        </div>
                        <div className={"block"}>
                            <label htmlFor={"nickname"}>Пароль:</label>
                            <input type="password" name={"password"} id={"password"} value={password}
                                   placeholder={"Введите пароль..."} onChange={this.handlePasswordInput}
                                   onKeyPress={this.handleEnterPress}/>
                        </div>

                        <div className={"info"}>
                            {error && <p className={"error"}>Ошибка: {error}</p>}
                            {info && <p className={"success"}>Информация: {info}</p>}
                        </div>
                    </main>
                    <footer>
                        <button type={"button"} onClick={this.okButtonClick}>{this.okButton}</button>
                        <button type={"button"} onClick={changeState}>{this.changeButton}</button>
                    </footer>
                </div>
            </div>
        );
    }
}

export default Enter;
