import React from 'react';
import ReactDOM from 'react-dom';
import './index.sass';
import App from './App';
import * as serviceWorker from './serviceWorker';
import RootStore from "./store";
import {Provider} from "mobx-react";

ReactDOM.render(
    <>
        <Provider root={new RootStore()}>
            <App/>
        </Provider>
    </>,
    document.getElementById('root'));

serviceWorker.register();
