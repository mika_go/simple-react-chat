import RootStore from "../index";
import {
    AuthMessage,
    ErrorMessage,
    Message,
    MessageTypes,
    OnlineListMessage,
    PrivateMessage,
    PublicMessage,
    Result
} from "./types";
import {observable} from "mobx";
import {ChatMessage} from "../ChatStore/types";

class ApiStore {
    root: RootStore;
    url: string;
    sock?: WebSocket;
    searchTimeout?: NodeJS.Timeout;
    @observable serverStatus: boolean = false;

    constructor(root: RootStore, url: string) {
        this.root = root;
        this.url = url;

        this.connect();
    }

    connect = () => {
        this.sock = new WebSocket(this.url);

        this.sock.onopen = this.onOpen;
        this.sock.onmessage = this.onMessage;
        this.sock.onclose = this.onClose;
    };

    onOpen = () => {
        if (this.searchTimeout) clearTimeout(this.searchTimeout);
        console.log("Connection open");
        this.serverStatus = true;
    };

    onMessage = (ev: MessageEvent) => {
        const data = JSON.parse(ev.data) as Message;
        data.message_type = +data.message_type;
        let payload;
        let message: ChatMessage;
        switch (data.message_type) {
            case MessageTypes.Public:
                payload = data.payload as PublicMessage;
                message = {
                    from: payload.from,
                    text: payload.message,
                    time: new Date(),
                    selected: false
                };
                this.root.chatStore.addMessage(message);
                break;
            case MessageTypes.Private:
                payload = data.payload as PrivateMessage;
                message = {
                    from: payload.from,
                    text: payload.message,
                    time: new Date(),
                    selected: false
                };
                this.root.chatStore.addPrivateMessage(message);
                break;
            case MessageTypes.OnlineList:
                payload = data.payload as OnlineListMessage;
                this.root.chatStore.updateUsers(payload.users);
                break;
            default:
                break;
        }
    };

    onClose = (ev: CloseEvent) => {
        if (ev.wasClean) {
            console.warn("Connection clean closed: " + ev.code + " " + ev.reason);
        } else {
            console.warn("Connection lost: " + ev.code + " " + ev.reason)
        }
        this.serverStatus = false;
        this.searchTimeout = setTimeout(() => {
            this.connect();
        }, 1000);
    };

    authorize = (nickname: string, password: string): Result | undefined => {
        if (!this.sock) return;
        const authPayload: AuthMessage = {nickname: nickname, password: password};
        const message: Message = {message_type: MessageTypes.Auth, payload: authPayload};
        this.sock.send(JSON.stringify(message));

        return this.handleResult();
    };

    register = (nickname: string, password: string): Result | undefined => {
        if (!this.sock) return;
        const registerPayload: AuthMessage = {nickname: nickname, password: password};
        const message: Message = {message_type: MessageTypes.Register, payload: registerPayload};
        this.sock.send(JSON.stringify(message));

        return this.handleResult();
    };

    logout = (): Result | undefined => {
        if (!this.sock) return;
        const message: Message = {message_type: MessageTypes.Logout};
        this.sock.send(JSON.stringify(message));

        return this.handleResult();
    };

    sendPublic = (from: string, text: string): Result | undefined => {
        if (!this.sock) return;
        const publicPayload: PublicMessage = {from: from, message: text};
        const message: Message = {message_type: MessageTypes.Public, payload: publicPayload};
        this.sock.send(JSON.stringify(message));

        return this.handleResult();
    };

    forwardPublic = (from: string, text: string): Result | undefined => {
        if (!this.sock) return;
        const publicPayload: PublicMessage = {from: from, message: text, is_forwarded: true};
        const message: Message = {message_type: MessageTypes.Public, payload: publicPayload};
        this.sock.send(JSON.stringify(message));

        return this.handleResult();
    };

    sendPrivate = (from: string, to: string, text: string) => {
        if (!this.sock) return;
        const publicPayload: PrivateMessage = {from: from, to: to, message: text};
        const message: Message = {message_type: MessageTypes.Private, payload: publicPayload};
        this.sock.send(JSON.stringify(message));

        return this.handleResult();
    };

    forwardPrivate = (from: string, to: string, text: string): Result | undefined => {
        if (!this.sock) return;
        const privatePayload: PrivateMessage = {from: from, to: to, message: text, is_forwarded: true};
        const message: Message = {message_type: MessageTypes.Private, payload: privatePayload};
        this.sock.send(JSON.stringify(message));

        return this.handleResult();
    };

    handleResult = (): Result => {
        const result = new Result();
        const listener = (ev: MessageEvent) => {
            const data = JSON.parse(ev.data) as Message;
            data.message_type = +data.message_type;
            if (data.message_type === MessageTypes.Ok) {
                result.result = true;
            } else if (data.message_type === MessageTypes.Error) {
                const payload = data.payload as ErrorMessage;
                result.code = +payload.code;
                result.message = payload.message;
                result.result = false;
            } else {
                return
            }
            this.sock!.removeEventListener("message", listener);
        };
        setTimeout(() => {
            this.sock!.removeEventListener("message", listener);
        }, 500);
        this.sock!.addEventListener("message", listener);
        return result;
    }
}

export default ApiStore;
