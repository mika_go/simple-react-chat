import {observable} from "mobx";

export enum MessageTypes {
    Error = 0,
    Ok,
    Auth,
    Register,
    Logout,
    Public,
    Private,
    OnlineList
}

export enum ApiErrors {
    BadRequest = 400,
    Unauthorized = 401,
    InternalServerError = 500,
    BadGateway = 502
}

export interface Message {
    message_type: MessageTypes;
    payload?: Payload;
}

export interface Payload {
}

export interface ErrorMessage extends Payload {
    code: ApiErrors;
    message: string;
}

export interface AuthMessage extends Payload {
    nickname: string;
    password: string;
}

export interface PublicMessage extends Payload {
    from: string;
    message: string;
    is_forwarded?: boolean;
}

export interface PrivateMessage extends PublicMessage {
    to: string;
}

export interface OnlineListMessage extends Payload {
    users: string[];
}

export class Result {
    @observable result?: boolean;
    code: ApiErrors = 0;
    message: string = "";
}
