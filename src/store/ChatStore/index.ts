import {action, computed, IObservableArray, observable, observe, values} from "mobx";
import RootStore from "../index";
import {ChatMessage, chatMessageToString, GENERAL_CHAT, User} from "./types";
import {Result} from "../ApiStore/types";

class ChatStore {
    root: RootStore;
    @observable nickname: string = "";
    @observable text: string = "";
    @observable users: User[] = [];
    @observable messages: Map<string, ChatMessage[]> = new Map<string, ChatMessage[]>();
    @observable currentChatName: string = GENERAL_CHAT;
    @observable selectMode: boolean = false;
    @observable selectedMessages: ChatMessage[] = [];
    @observable newMessage: boolean = false;

    constructor(root: RootStore) {
        this.root = root;
        this.messages.set(GENERAL_CHAT, []);

        observe(this.selectedMessages, change => {
            console.log(change.object);
        })
    }

    @action
    reset = () => {
        this.nickname = "";
        this.text = "";
        (this.users as IObservableArray).clear();
        this.messages.clear();
        this.messages.set(GENERAL_CHAT, []);
        this.currentChatName = GENERAL_CHAT;
        this.selectMode = false;
        (this.selectedMessages as IObservableArray).clear();
        this.newMessage = false;
    };

    @computed
    get currentChat(): ChatMessage[] {
        const chat = this.messages.get(this.currentChatName);
        if (chat) {
            return chat;
        }
        return [];
    }

    @computed
    get sortedCurrentChat(): ChatMessage[] {
        return this.currentChat.slice().sort((a, b) => a.time.getTime() - b.time.getTime());
    }

    @computed
    get sortedSelectedMessages(): ChatMessage[] {
        return this.selectedMessages.slice().sort((a, b) => a.time.getTime() - b.time.getTime());
    }

    @computed
    get sortedUsers(): User[] {
        return this.users.slice().sort((a, b) => {
            if (a.nickname < b.nickname) {
                return -1;
            }
            if (a.nickname > b.nickname) {
                return 1;
            }
            return 0;
        });
    }

    @action
    setCurrentChat = (chatName: string) => {
        if (chatName === this.nickname) return;
        this.currentChatName = chatName;
        if (chatName === GENERAL_CHAT) {
            this.newMessage = false;
        } else {
            const user = this.users.find(value => value.nickname === chatName);
            if (user) {
                user.newMessage = false;
            }
        }
    };

    @action
    logout = () => {
        const result = this.root.apiStore.logout();
        if (!result) return;
        const observeResult = observe(result, "result", change => {
            const value = change.newValue as boolean;
            if (value) {
                this.root.enterStore.reset();
                this.reset();
            }

            observeResult();
        })
    };

    @action
    updateUsers = (users: string[]) => {
        if (users.length > this.users.length) {
            for (let user of users) {
                if (!this.users.find(value => value.nickname === user)) {
                    this.users.push({nickname: user, newMessage: false});
                    this.messages.set(user, []);
                }
            }
        } else if (users.length < this.users.length) {
            for (let user of this.users) {
                if (!users.find(value => value === user.nickname)) {
                    if (this.currentChatName === user.nickname) this.currentChatName = GENERAL_CHAT;
                    (this.users as IObservableArray).remove(user);
                    this.messages.delete(user.nickname);
                }
            }
        }
    };

    @action
    addMessage = (message: ChatMessage) => {
        const chat = this.messages.get(GENERAL_CHAT);
        if (chat) {
            chat.push(message);
            if (this.currentChatName !== GENERAL_CHAT) {
                this.newMessage = true;
            }
        }
    };

    @action
    addPrivateMessage = (message: ChatMessage) => {
        const chat = this.messages.get(message.from);
        if (chat) {
            chat.push(message);
            if (this.currentChatName !== message.from) {
                const user = this.users.find(value => value.nickname === message.from);
                if (user) {
                    user.newMessage = true;
                }
            }
        }
    };

    @action
    selectMessage = (message: ChatMessage) => {
        this.selectedMessages.push(message);
    };

    @action
    unselectMessage = (message: ChatMessage) => {
        (this.selectedMessages as IObservableArray).remove(message);
    };

    @action
    toggleSelectMode = () => {
        if (this.selectMode) {
            (this.selectedMessages as IObservableArray).clear();
            this.messages.forEach(messages => messages.forEach(value => value.selected = false));
        }
        this.selectMode = !this.selectMode;
    };

    @action
    sendMessage = () => {
        if (this.text.trim() === "") {
            this.text = "";
            return;
        }
        let result: Result | undefined;
        const time = new Date();
        const message: ChatMessage = {from: this.nickname, text: this.text, time: time, selected: false};
        if (this.currentChatName === GENERAL_CHAT) {
            result = this.root.apiStore.sendPublic(this.nickname, this.text);
        } else {
            result = this.root.apiStore.sendPrivate(this.nickname, this.currentChatName, this.text);
        }
        if (!result) return;
        const observeResult = observe(result, "result", change => {
            const value = change.newValue as boolean;
            if (value) {
                this.currentChat.push(message);
                this.text = "";
            }

            observeResult();
        });
    };

    @action
    forwardMessages = () => {
        if (this.selectedMessages.length === 0) return;
        let i = 0;
        const next = (): ChatMessage | undefined => {
            if (i !== this.sortedSelectedMessages.length) {
                return this.sortedSelectedMessages[++i];
            }
            return;
        };
        this.forwardMessage(
            this.currentChatName,
            this.sortedSelectedMessages[i],
            this.currentChatName === GENERAL_CHAT,
            next
        );
    };

    @action
    forwardMessage = (to: string, message: ChatMessage, privateMessage: boolean, next: () => ChatMessage | undefined) => {
        let result: Result | undefined;
        if (privateMessage) {
            result = this.root.apiStore.forwardPublic(
                this.nickname,
                chatMessageToString(message, false)[0]
            );
        } else {
            result = this.root.apiStore.forwardPrivate(
                this.nickname,
                to,
                chatMessageToString(message, false)[0]
            );
        }
        if (!result) return;
        const observeResult = observe(result, "result", change => {
            const value = change.newValue as boolean;
            if (value) {
                const newMessage: ChatMessage = {
                    from: this.nickname,
                    text: chatMessageToString(message, false)[0],
                    time: new Date(),
                    selected: false
                };
                this.currentChat.push(newMessage);
                const nextMessage = next();
                if (!nextMessage) {
                    (this.selectedMessages as IObservableArray).clear();
                    this.toggleSelectMode();
                    return;
                }
                this.forwardMessage(to, nextMessage, privateMessage, next);
            }

            observeResult();
        });
    };
}

export default ChatStore;
