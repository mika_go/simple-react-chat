export const GENERAL_CHAT = "Общий чат";

export interface ChatMessage {
    from: string;
    text: string;
    time: Date;
    selected: boolean;
}

export interface User {
    nickname: string;
    newMessage: boolean;
}

export function chatMessageToString(message: ChatMessage, determine: boolean): string[] {
    if (determine) return  [`${message.from}, ${message.time.toLocaleTimeString()}:`, `${message.text}`];
    return [`${message.from}, ${message.time.toLocaleTimeString()}: ${message.text}`];
}
