import {action, observable, observe} from "mobx";
import {CurrentState} from "./types";
import RootStore from "../index";
import {ApiErrors} from "../ApiStore/types";

class EnterStore {
    root: RootStore;
    @observable state: CurrentState = CurrentState.Authorization;
    @observable nickname: string = "";
    @observable password: string = "";
    @observable error: string = "";
    @observable info: string = "";
    @observable authorized: boolean = false;

    constructor(root: RootStore) {
        this.root = root;
    }

    @action
    reset = () => {
        this.state = CurrentState.Authorization;
        this.nickname = "";
        this.password = "";
        this.error = "";
        this.info = "";
        this.authorized = false;
    };

    @action
    changeState = () => {
        this.password = "";
        this.info = "";
        this.error = "";
        switch (this.state) {
            case CurrentState.Authorization:
                this.state = CurrentState.Registration;
                break;
            case CurrentState.Registration:
                this.state = CurrentState.Authorization;
                break;

        }
    };

    @action
    authorize = () => {
        this.error = "";
        const result = this.root.apiStore.authorize(this.nickname, this.password);
        if (!result) return;
        const observeResult = observe(result, "result", change => {
            const value = change.newValue as boolean;
            if (!value) {
                console.error(`Error(${result.code}): ${result.message}`);
                switch (result.code) {
                    case ApiErrors.BadRequest:
                        this.error = "пользователь с таким именем уже авторизован";
                        break;
                    case ApiErrors.Unauthorized:
                        this.error = "пользователь с таким именем не существует";
                        break;
                    default:
                        this.error = "что-то сломалось";
                }
            } else {
                this.root.chatStore.nickname = this.nickname.valueOf();
                this.authorized = true;
                this.nickname = "";
                this.password = "";
            }

            observeResult();
        })
    };

    @action
    register = () => {
        this.info = "";
        const result = this.root.apiStore.register(this.nickname, this.password);
        if (!result) return;
        const observeResult = observe(result, "result", change => {
            const value = change.newValue as boolean;
            if (!value) {
                console.error(`Error(${result.code}): ${result.message}`);
                switch (result.code) {
                    case ApiErrors.Unauthorized:
                        this.error = "пользователь с таким именем уже зарегистрирован";
                        break;
                    default:
                        this.error = "что-то сломалось";
                }
            } else {
                this.error = "";
                this.state = CurrentState.Authorization;
                this.info = "Вы были успешно зарегистрированы";
            }

            observeResult();
        })
    };

}

export default EnterStore;
