import ApiStore from "./ApiStore";
import EnterStore from "./EnterStore";
import ChatStore from "./ChatStore";

class RootStore {
    apiStore: ApiStore;
    enterStore: EnterStore;
    chatStore: ChatStore;

    constructor() {
        this.apiStore = new ApiStore(this, "ws://localhost:7777/ws");
        this.enterStore = new EnterStore(this);
        this.chatStore = new ChatStore(this);
    }
}

export default RootStore;
